from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='rawgpy',
      version='1.0.5',
      description='RAWG.io python api wrapper',
      url='https://gitlab.com/Kononkov1998/rawg-python-wrapper',
      author='Kononkov1998',
      author_email='kononkov98@mail.ru',
      license='GPLv3',
      long_description=long_description,
      long_description_content_type="text/markdown",
      packages=['rawgpy', 'rawgpy.data_classes'],
      zip_safe=False,
          classifiers=[
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
          "Operating System :: OS Independent",
      ])


# pip install -e .
# to install locally

# python setup.py sdist bdist_wheel
# twine upload dist/*
