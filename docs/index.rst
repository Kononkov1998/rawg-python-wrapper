.. RAWGpy documentation master  file, created by
   sphinx-quickstart on Tue Jan 22 11:43:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RAWGpy's documentation!
==================================

These API docs use terminology from the RAWG API. Important to note may be:

slug
  The slug is the version of a name that was adjusted for use in URLs, spaces are replaced with hyphens and all characters are lowercase


.. toctree::
    :maxdepth: 2

    quickstart
    rawgpy.data_classes

.. autosummary::
    :toctree: modules

    rawgpy.rawg
    rawgpy.game
    rawgpy.base
    rawgpy.collection
    rawgpy.user
    rawgpy.utils



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


