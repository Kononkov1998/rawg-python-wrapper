{{ name | escape | underline}}

.. automodule:: {{ fullname }}
    :members:
