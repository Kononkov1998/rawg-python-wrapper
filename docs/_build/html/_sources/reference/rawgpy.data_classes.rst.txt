rawgpy.data\_classes package
============================

Submodules
----------

.. toctree::

   rawgpy.data_classes.charts
   rawgpy.data_classes.id_name_slug
   rawgpy.data_classes.platform_
   rawgpy.data_classes.rating
   rawgpy.data_classes.store

Module contents
---------------

.. automodule:: rawgpy.data_classes
    :members:
    :undoc-members:
    :show-inheritance:
