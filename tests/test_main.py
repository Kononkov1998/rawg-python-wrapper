import rawgpy


class TestRAWG:
    def setup(self):
        self.user_agent = "testing-rawgpy"
        self.rawg = rawgpy.RAWG(self.user_agent)

    def test_search_request(self):
        search_res = self.rawg.search_request("Warframe")
        assert isinstance(search_res["results"][0]["name"], str)

    def test_game_search(self):
        res = self.rawg.search("Warframe")
        print([r.name for r in res])
        assert res[0].name == "Warframe"

    def test_singleton_behaviour(self):
        r = rawgpy.RAWG("")
        assert r.user_agent == self.user_agent


class TestGame:
    def setup(self):
        self.rawg = rawgpy.RAWG("testing-rawgpy")

    def test_json_conversion(self):
        self.game = rawgpy.game.Game(
            {"id": 123, "name": "Warframe", "slug": "warframe"})
        assert self.game.name == "Warframe"

    def test_game_populate(self):
        res = self.rawg.search("Warframe")[0]
        res.populate()
        assert isinstance(res.stores[0], rawgpy.store.Store)
