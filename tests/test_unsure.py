import datetime
import re

import rawgpy


#
# the following is a rough idea of how im going to make the rawg API wrapper
#
# 1. i did not include every variable, since those are too numerous to describe
# 2. "methodname - -()" denotes that there should be a variable AND a method for this
# 3. im mostly going to cover the most important features


class TestGame:
    def setup(self):
        self.rawg = rawgpy.RAWG("testing-rawgpy")
        self.game = self.rawg.get_game("warframe")

    def test_setup(self):
        assert isinstance(self.rawg, rawgpy.RAWG)

    def test_multiton_behaviour(self):
        self.rawg.get_game("portal")
        assert self.game is self.rawg.get_game("warframe")

    def test_search(self):
        game = self.rawg.search("Warframe")[0]  # type Game
        assert isinstance(game, rawgpy.game.Game)

    def test_game_populate(self):
        # populate Game with information retrieved from rawg, this method should exist for every type of object
        assert self.game.populate()

    def test_in_collection(self):
        # in which collections is this game, var if only shown, func if all
        c = self.game.collections[0]
        assert isinstance(c, rawgpy.collection.Collection)

    def test_categorize(self):
        sucess = self.game.categorize(rawgpy.Category(""))  # TODO: category
        assert sucess

    def test_wishlist(self):
        sucess = self.game.wishlist()
        assert sucess

    def test_collection(self):
        sucess = self.game.collection("")
        assert sucess

    # all of these have name_length
    def test_suggestions(self):
        game = next(self.game.suggestions)  # var if only shown, func if all
        assert isinstance(game, rawgpy.game.Game)

    def test_screenshots(self):
        img = self.game.screenshots[0]
        # TODO: check image regex
        assert re.match(r"https?://.*?[jpg|jpeg|gif|png]", img)

    def test_videos(self):
        vid = self.game.videos[0]
        # TODO: check image regex
        assert re.match(r"https?://.*?[mp4|webm]", img)

    def test_achivements(self):
        ach = self.game.achivements[0]
        assert ach  # TODO: find way to identify achivements

    def test_posts(self):
        post = self.game.posts[0]
        # TODO: decide if this should be class or not
        assert isinstance(post, rawgpy.Post)

    def test_streams(self):
        stream = self.game.streams[0]
        # TODO: check regex
        assert re.match(r"https?://.*?[youtube|twitch]")

    def test_updates(self):
        update = self.game.updates[0]
        assert update  # TODO: find way to assert updates

    def test_reviews(self):
        reviews = self.game.reviews  # unpopulated review objects
        assert isinstance(reviews, rawgpy.Reviews)

    def test_comments(self):
        comments = self.game.comments  # unpopulated comments objects
        assert isinstance(comments, rawgpy.Comments)

    def test_screensots(self):
        # has to have identifier accessible
        screenshot = self.game.screenshots[0]
        # TODO: check screenshot regex
        assert re.match(r"https?://.*?[jpg|jpeg|gif|png]", screenshot)

    def test_creators(self):
        creator = self.game.creators
        assert isinstance(creator.name, str)  # TODO: better creator assert

    def test_stores(self):
        store = self.game.stores[0]  # list of store URLs
        assert isinstance(store.url, str)

    def test_game_edit(self):
        alternative_titles = ["t1", "t2"]
        about = "t123"
        release_date = datetime.now()
        esrb = "test"
        metascore = "test"
        platform = "test"
        genres = ["test"]
        publishers = ["test"]
        developers = ["test"]
        website = "www.test.com"
        reddit = "r/test"
        self.game.edit(
            alternative_titles=alternative_titles,
            about=about,
            release_date=release_date,
            esrb=esrb,
            metascore=metascore,
            platform=platform,
            genres=genres,
            publishers=publishers,
            developers=developers,
            website=website,
            reddit=reddit
        )
        assert alternative_titles == self.game.alternative_titles and \
               about == self.game.about and \
               release_date == self.game.release_date and \
               esrb == self.game.esrb and \
               metascore == self.game.metascore and \
               platform == self.game.platform and \
               genres == self.game.genres and \
               publishers == self.game.publishers and \
               developers == self.game.developers and \
               website == self.game.website and \
               reddit == self.game.reddit

    def test_edit_stores(self):
        store = "https://test-store.url/game"
        self.game.edit_stores(
            add_store=store
        )
        added = store in self.game.stores
        self.game.edit_stores(
            remove_store=store
        )
        removed = store not in self.game.stores
        assert added and removed

    def test_edit_tags(self):
        tag = "test-tag"
        self.game.edit_tags(
            add_tag=tag
        )
        added = tag in self.game.tags
        self.game.edit_tags(
            remove_tag=tag
        )
        removed = not tag in self.game.tags
        assert added and removed

    def test_add_screenshot(self):
        url = "https://i.imgur.com/fXebSN9.png"
        self.game.add_screenshot(url)
        assert url in self.game.screenshots

    def test_replace_screenshot(self):
        url = "https://i.imgur.com/fXebSN9.png"
        identifier = self.game.screenshots[-1]
        self.game.replace_screenshot(url, identifier)
        assert url in self.game.screenshots

    def test_remove_screenshot(self):
        identifier = self.game.screenshots[-1]
        self.game.remove_screenshot(identifier)
        assert url in self.game.screenshots


class TestUser:
    def setup(self):
        self.rawg = rawgpy.RAWG("testing-rawgpy")
        self.user = self.rawg.get_user("laundmo_taw")

    def test_user(self):
        slug = "laundmo_taw"
        user = self.rawg.get_user(slug)  # name, @name, or user url
        assert user.slug == slug

    def test_bio(self):
        assert isinstance(self.user.bio, str)

    def test_playing(self):
        assert isinstance(self.user.playing[0], rawgpy.game.Game)

    def test_owned(self):
        assert isinstance(self.user.owned[0], rawgpy.game.Game)

    def test_beaten(self):
        assert isinstance(self.user.beaten[0], rawgpy.game.Game)

    def test_dropped(self):
        assert isinstance(self.user.dropped[0], rawgpy.game.Game)

    def test_toplay(self):
        assert isinstance(self.user.toplay[0], rawgpy.game.Game)

    def test_yet(self):
        assert isinstance(self.user.yet[0], rawgpy.game.Game)

# user.bio  # biography
# user.favourites  # list of unpopulated Game
# user.gamestats
# user.reviewstats
# user.collectionstats
# user.developerstats
# user.genrestats
# user.recently_added
# user.library  # dict: categoryname: list_of_unpopulated_Game
# user.wishlist  # dict: list_of_unpopulated_Game
# user.reviews  # alternatively, ratings
# user.collections  # list of collection obj (unpopulated)
# user.following  # list of users this user is following
# user.followers  # list of followers this user has
#
# user.logged_in()  # check if token is known
# user.edit(
#    full_name="current",
#    username="current",
#    bio="current"
#    delete_avatar=False,
# )
#
# collection = rawg.collection(title)
# collection.creator
# collection.created_at
# collection.title
# collection.games - -()  # list of unpopulated games
# collection.edit(
#    title="current",
#    description="current",
#    delete=False
# )
# collection.add(game)  # or identifier/slug?
# collection.upvote
# collection.downvote
# collection.votes
# collection.recommended_collections  # unpopulated Collection objects
#
#
# dev = game.developer  # type Store
# dev.populate()
# dev.games  # list of Game object (unpopulated)
#
